import telebot
from telebot import types

bot = telebot.TeleBot("")


@bot.message_handler(commands=["start"])
def start(message: types.Message):
    print(message.from_user.id)
    res = bot.get_chat_member(chat_id='@chatname', user_id=message.from_user.id)
    bot.send_message(message.from_user.id, res.status)


if __name__ == '__main__':
    bot.polling(non_stop=True)

