from aiogram import Bot, Dispatcher, types
from aiogram.utils import executor

bot = Bot(token="token")

dp = Dispatcher(bot)


@dp.message_handler(commands=['start'])
async def send_welcome(message: types.Message):
    try:
        result = await bot.get_chat_member(chat_id='@chatname', user_id=message.from_user.id)
        await message.answer(result.status)
    except:
        print('error')


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
